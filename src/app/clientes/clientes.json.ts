import {Cliente} from './cliente';

export const CLIENTES: Cliente[] = [
  {id: 1, nombre: 'Andrés', apellido: 'Guzmán', email: 'profesor@fakemail.fake', createAt: '2017-12-11'},
  {id: 2, nombre: 'John', apellido: 'Doe', email: 'JohnDoe@fakemail.fake', createAt: '2017-10-08'},
  {id: 3, nombre: 'Linus', apellido: 'Torvards', email: 'Linux@fakemail.fake', createAt: '2017-07-19'},
  {id: 4, nombre: 'Frodo', apellido: 'Baggins', email: 'FrodoBaggins@fakemail.fake', createAt: '2017-02-01'},
  {id: 5, nombre: 'Darth', apellido: 'Vader', email: 'DarthVader@fakemail.fake', createAt: '2017-09-27'},
  {id: 6, nombre: 'Stan', apellido: 'Lee', email: 'marvel@fakemail.fake', createAt: '2017-10-30'},
  {id: 7, nombre: 'Ralph', apellido: 'Lauren', email: 'RalphLauren@fakemail.fake', createAt: '2017-01-02'},
  {id: 8, nombre: 'James', apellido: 'Bond', email: '007@fakemail.fake', createAt: '2017-03-15'},
  {id: 9, nombre: 'Chuck', apellido: 'Norris', email: 'ChuckNorris@fakemail.fake', createAt: '2017-04-24'},
  {id: 10, nombre: 'John', apellido: 'Lemon', email: 'lemon@fakemail.fake', createAt: '2017-05-12'}
];
