import { Component, OnInit } from '@angular/core';
import {Cliente} from './cliente';
import {ClienteService} from './cliente.service';
import {ActivatedRoute, Router} from '@angular/router';
import swal from 'sweetalert2';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private cliente: Cliente = new Cliente();
  private titulo = 'Crear cliente';
  constructor(private clienteService: ClienteService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cargarCliente();
  }
  cargarCliente(): void {
    this.activatedRoute.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.clienteService.getCliente(id).subscribe((cliente) => this.cliente = cliente);
      }
    });
  }
  public create(): void {
    this.clienteService.create(this.cliente).subscribe(json => {
      this.router.navigate(['/clientes'])
      swal.fire('Nuevo cliente', `Cliente ${json.cliente.nombre} creado con éxito`, 'success')
    }
    );
    console.log('Clicked!');
    console.log(this.cliente);
  }

  update(): void {
    this.clienteService.update(this.cliente).subscribe(json => {
      this.router.navigate(['/clientes'])
      swal.fire('Cliente actualizado',
        `Cliente ${json.cliente.nombre} actualizado con éxito`, 'success')
    });
  }

}
